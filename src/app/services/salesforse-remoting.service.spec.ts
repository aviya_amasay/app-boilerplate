import { TestBed } from '@angular/core/testing';

import { SalesforseRemotingService } from './salesforse-remoting.service';

describe('SalesforseRemotingService', () => {
  let service: SalesforseRemotingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesforseRemotingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
