import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SalesforseRemotingService {

  private _remoteManager: any;
  constructor(@Inject('Window') private window: Window) {
    this._remoteManager = (window as any).Visualforce.remoting.Manager;
  }
  _exceptionHandler(event: any):String {
    return ''; //here should go all exception 
  }
    invoke(controller: string, method: string, ...args: Array<any>): Promise<any | Error> {
      let action: string = `${controller}.${method}`;
      return new Promise((resolve, reject) => {
        let cb = (result: any, event: { status: any; }): any => {
          event.status 
            ? resolve(result) 
            : reject(this._exceptionHandler(event));
        }
        let remotingParams: object = { escape: false };
        this._remoteManager.invokeAction(
          action,
          ...args, 
          cb, 
          remotingParams
        );
      });
  }
}
